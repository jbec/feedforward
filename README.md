#  Feed Forward Perceptron

This C++ code defines a multilayer perceptron and implement learning using a stochastic gradient backward propagation algorithm.

## Requirements

To compile :
* CMake (version 3.1 or higher)
* C++11 supportive compiler

## Installation

A Makefile to compile and install the code can be generated using the CMake build scripts.
CMake allows two build types: "release" and "debug".
The build type decides on compiler options and whether assertions are compiled.
Use "release" for optimal performance (e.g. for production runs) and
"debug" for optimal diagnostics (e.g. for code development).
Out of source builds are recommended.

```
mkdir build
cd build
cmake PATH_TO_SOURCE -DCMAKE_BUILD_TYPE=debug (/release) -DCMAKE_INSTALL_PREFIX=PATH_TO_INSTALL_DIR
make -j
make install
```
## Usage

After installation, several example programs are available in the directory bin/. The corresponding source codes are in the program/ directory.
At the moment there are two programs:
* `test_create`, which generate a network and write it to the hard drive
* `test_learn`, which learns to predict from a 2D input

Details on the options of the program can be obtained by typing `program_name --help` in a command window or a terminal.

### Some examples
In order to train a network with 4 internal layers of 6 neurons each to predict the function $`f(x_1,x_2) = 1` if `x_2 > 4 x_1 (1-x_1)`$, and store the resut in the directory `test1` type:
```
bin/test_learn -o test1 -nl 4 -nn 6 -fun 1
```
The output directory `test1` then contains different files:
* `test_learn.args` stores the inline command that was used to generate the data.
* `map0.txt` gives the output of the network $`N(x_1,x_2)`$ on a mesh of size $`100\times100`$ of uniformly distributed points in $`(x_1,x_2)`$.
* `error.txt` contains the evolution of the squared $`L_2`$ error defined as $`\int_{[0,1]^2} (N(x_1,x_2)-f(x_1,x_2))^2\,\mathrm{d}x_1\,\mathrm{d}x_2`$ as a function of the number of epochs.
* `derr_x1_0.txt` and `derr_x2_0.txt` contain the two components of the network gradient $`(\partial_{x_1}N,  \partial_{x_2}N)`$ on the same $`100\times100`$ grid.

### Typical results

Time evolution of the error (as a function of the number of epochs)

![time evolution error](./pictures/fig_error.jpg) 

Final map given by the network

![final result - map](./pictures/fig_map.jpg)
