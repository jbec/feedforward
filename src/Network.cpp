//
//  Network.cpp
//  FeedForward
//
//  Created by Jeremie Bec
//

#include "Network.hpp"

Network::Network(size_t ninput, size_t noutput, size_t nneurons, size_t nlayer, std::default_random_engine& rng, testfun fun) {
    ninput_=ninput;
    noutput_=noutput;
    nlayer_=nlayer;
    nperlayer_ = nneurons;
    Net_ = vector<Layer>(nlayer+2);
    Net_.at(0) = Layer(ninput_,nperlayer_,Logistic,rng);
    for(size_t k=1; k<=nlayer_; ++k)
        Net_.at(k) = Layer(nperlayer_,nperlayer_,Logistic,rng);
    Net_.at(nlayer_+1) = Layer(nperlayer_,noutput_,Logistic,rng);
    fun_ = fun;
}

Network::Network(FILE *fin, testfun fun) {
    // Read metadata
    double meta[4];
    if(fread(meta, sizeof(double), 4, fin)==4) {
        ninput_ = (size_t)meta[0];
        noutput_ = (size_t)meta[1];
        nlayer_ = (size_t)meta[2];
        nperlayer_ = (size_t)meta[3];
        Net_ = vector<Layer>(nlayer_+2);
        
        vector<double> W, B;
        W.resize(ninput_*nperlayer_);
        B.resize(nperlayer_);
        
        fread(&W[0], sizeof(double), ninput_*nperlayer_, fin);
        fread(&B[0], sizeof(double), nperlayer_, fin);
        Net_.at(0) = Layer(ninput_,nperlayer_,W,B,Logistic);
        W.resize(nperlayer_*nperlayer_);
        for(size_t k=1; k<=nlayer_; ++k) {
            fread(&W[0], sizeof(double), nperlayer_*nperlayer_, fin);
            fread(&B[0], sizeof(double), nperlayer_, fin);
            Net_.at(k) = Layer(nperlayer_,nperlayer_,W,B,Logistic);
        }
        W.resize(nperlayer_*noutput_);
        B.resize(noutput_);
        fread(&W[0], sizeof(double), nperlayer_*noutput_, fin);
        fread(&B[0], sizeof(double), noutput_, fin);
        Net_.at(nlayer_+1) = Layer(nperlayer_,noutput_,W,B,Logistic);
        fun_ = fun;
    }
}

vector <double> Network::Eval(vector<double> X) {
    if(X.size()!=ninput_)
        ErrorMsg("Error in Network::Eval: wrong size of input");
    vector<double> a = X;
    for(size_t k=0; k<=nlayer_+1; ++k) {
        Net_.at(k).Eval(a);
        a = Net_.at(k).Out();
    }
    return a;
}

void Network::BackProp(vector<double> ytarg, vector<double> yout, double lamb) {
    if(ytarg.size()!=noutput_)
        ErrorMsg("Error in Network::BackProp: wrong size of target");
    if(yout.size()!=noutput_)
        ErrorMsg("Error in Network::BackProp: wrong size of output");
    vector<double> delt(noutput_);
    for(size_t i=0; i<noutput_; ++i)
        delt.at(i) = 2.0*(yout.at(i)-ytarg.at(i));
    Net_.at(nlayer_+1).SetDelta(delt);
    Net_.at(nlayer_+1).UpGrade(lamb);
    for(size_t k=0; k<nlayer_+1; ++k) {
        Net_.at(nlayer_-k).SetDelta(Net_.at(nlayer_-k+1).GetDelta());
        Net_.at(nlayer_-k).UpGrade(lamb);
    }
}

vector<double> Network::FwdError() {
    vector<double> M(ninput_*ninput_,0.0);
    vector<double> W, Mnew;
    size_t nl, nr;
    for(size_t i=0; i<ninput_; ++i)
        M.at(i*ninput_+i) = 1.0;
    for(size_t k=0; k<nlayer_+2; ++k) {
        nl = Net_.at(k).Nout();
        nr = Net_.at(k).Nin();
        W = Net_.at(k).W();
        Mnew = vector<double>(nl*ninput_);
        for(size_t j=0; j<ninput_; ++j)
            for(size_t i=0; i<nl; ++i) {
                double df = Net_.at(k).fdiff(i);
                for(size_t l=0; l<nr; ++l)
                    Mnew.at(i*ninput_+j) += df*W.at(i*nr+l)*M.at(l*ninput_+j);
            }
        M = Mnew;
    }
    return M;
}

void Network::Save(const std::string& fname) {
    char cname[512];
    strcpy(cname, fname.c_str());
    FILE *fout = fopen(cname,"a");
    // Metadata
    double meta[4];
    meta[0] = (double)ninput_;
    meta[1] = (double)noutput_;
    meta[2] = (double)nlayer_;
    meta[3] = (double)nperlayer_;
    fwrite(meta, sizeof(double), 4, fout);
    // Data layer per layer
    vector<double> W, B;
    for(size_t k=0; k<=nlayer_+1; ++k) {
        W = Net_.at(k).W();
        fwrite(&W[0], sizeof(double), Net_.at(k).Nin()*Net_.at(k).Nout(), fout);
        B = Net_.at(k).B();
        fwrite(&B[0], sizeof(double), Net_.at(k).Nout(), fout);
    }
    fclose(fout);
}

double Network::calcL2(int Nx) {
    vector<double> x(2), res;
    double l2 = 0;
    
    for(int ix=0; ix<Nx; ++ix) {
        for(int iy=0; iy<Nx; ++iy) {
            x.at(0) = (double)ix/(double)(Nx-1);
            x.at(1) = (double)iy/(double)(Nx-1);
            res = Eval(x);
            l2 += pow(res[0]-test_fun(x),2);
        }
    }
    return l2/(double)(Nx*Nx);
}

