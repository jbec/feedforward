//
//  Layer.hpp
//  FeedForward
//
//  Created by Jeremie Bec
//

#ifndef Layer_hpp
#define Layer_hpp

#include "basics/Utilities.hpp"

// Define various possibilities for the activation function
enum activfun { Logistic, ATan, ReLU, ELU1};

using namespace std;

class Layer {
public:
    // Empty creator
    Layer() { };
    // Creator with nin inputs, nout outputs, random weights, and activation function f
    Layer(size_t nin, size_t nout, activfun f, std::default_random_engine& rng);
    // Creator with nin inputs, nout outputs, given weights, and activation function f
    Layer(size_t nin, size_t nout, vector<double> W, vector<double> B, activfun f);
    // Class Destructor
    ~Layer();
    // Evaluate from input X
    void Eval(vector<double> X);
    // Compute the derivative of the activation function at Neuron i
    double fdiff(int i);
    // Multiply the error by the derivative of the activation function
    void SetDelta(vector<double> Delt);
    // Propagate the error backward multiplying by W
    vector<double> GetDelta();
    // UpGrade weigths with a rate lamb
    void UpGrade(double lamb);
    // Save the layer
    void Save(const std::string& fname);
    // Return output
    inline vector<double> Out() { return A_; };
    inline size_t Nout() { return nout_; };
    inline size_t Nin() { return nin_; };
    inline vector<double> W() { return W_; };
    inline vector<double> B() { return b_; };
private:
    // Size of input
    size_t nin_=0;
    // Size of output
    size_t nout_=0;
    // Weights
    vector<double> W_;
    // Biases
    vector<double> b_;
    // Input
    vector<double> X_;
    // Intermediate Values
    vector<double> Z_;
    // Output
    vector<double> A_;
    //
    vector<double> Delt_;
    // Activation function
    activfun f_;
};

#endif /* Layer_hpp */
