//
//  Network.hpp
//  FeedForward
//
//  Created by Jeremie Bec
//

#ifndef Network_hpp
#define Network_hpp

#include "Layer.hpp"

enum testfun { linear, parabola, circle, fractal };

class Network {
public:
    // Empty creator
    Network() {};
    // Create a network with ninput, nlayer with nneurons  and ReLU activation, a last layer with sigmoid and 1 output
    Network(size_t ninput, size_t noutput, size_t nneurons, size_t nlayer, std::default_random_engine& rng, testfun fun=linear);
    // Create a network by reading from a file stream
    Network(FILE *fin, testfun fun=linear);
    // Evaluate the output of the network from the input X
    vector<double> Eval(vector<double> X);
    // Backpropagation algorithm
    void BackProp(vector<double> ytarg, vector<double> yout, double lamb);
    // Forward evaluation of errors
    vector<double> FwdError();
    // Save the network
    void Save(const std::string& fname);
    // Compute the global L2 norm of the network
    double calcL2(int Nx);
    // Example of test function
    inline double test_fun(vector<double> x) {
        double f;
        switch (fun_) {
            case linear:
                f = (x.at(1)>x.at(0))?1.0:0.0;
                break;
            case parabola:
                f = (x.at(1)>4.0*x.at(0)*(1.0-x.at(0)))?1.0:0.0;
                break;
            case circle:
                f = (pow(x.at(1)-0.5,2)+pow(x.at(0)-0.5,2)>0.1)?1.0:0.0;
                break;
            case fractal: {
                    double test = 1e5;
                    double xx = x.at(0), yy = x.at(1)-0.5;
                    while(fabs(yy)<test) {
                        yy = 1.5*yy + 0.3*cos(2.0*M_PI*xx);
                        xx = 3.0*xx;
                        xx -= floor(xx);
                    }
                    f = (yy>0)?1:0;
                }
                break;
            default:
                ErrorMsg("Undefined test function");
                break;
        }
        return f;
    };
    inline size_t Nin() { return ninput_; };
    inline size_t Nout() { return noutput_; };
    inline size_t Nlayer() { return nlayer_; };
    inline size_t Nneurons() { return nperlayer_; };

private:
    // Number of input layers
    size_t ninput_=0;
    // Number of output layers
    size_t noutput_=0;
    // Number of layers
    size_t nlayer_=0;
    // Number of neuron per layer
    size_t nperlayer_=0;
    // Array of layers
    vector<Layer> Net_;
    // Type of test function
    testfun fun_=linear;
};

#endif /* Network_hpp */
