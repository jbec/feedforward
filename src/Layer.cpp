//
//  Layer.cpp
//  FeedForward
//
//  Created by Jeremie Bec
//

#include "Layer.hpp"

Layer::Layer(size_t nin, size_t nout, activfun f, std::default_random_engine& rng) {
    nin_ = nin;
    nout_ = nout;
    W_ = vector<double>(nin_*nout_);
    b_ = vector<double>(nout_);
    X_ = vector<double>(nin_);
    Z_ = vector<double>(nout_);
    A_ = vector<double>(nout_);
    Delt_ = vector<double>(nout_);
    f_ = f;
    cout<<"-layer "<<nin<<"x"<<nout<<" of type "<<f<<endl;
    // Initialization of weights
    std::normal_distribution<double> N(0.0,1.0);
    for(size_t i=0; i<nin_*nout; ++i)
        W_.at(i) = sqrt(2)*N(rng)/sqrt((double)nin_);
    // Initialization of biases
    for(size_t i=0; i<nout; ++i)
        b_.at(i) = 0;
        
}

Layer::Layer(size_t nin, size_t nout, vector<double> W, vector<double> B, activfun f) {
    nin_ = nin;
    nout_ = nout;
    if(W.size()!=nin_*nout_)
        ErrorMsg("Layer initialization: W has the wrong size");
    W_ = W;
    if(B.size()!=nout_)
        ErrorMsg("Layer initialization: B has the wrong size");
    b_ = B;
    X_ = vector<double>(nin_);
    Z_ = vector<double>(nout_);
    A_ = vector<double>(nout_);
    Delt_ = vector<double>(nout_);
    f_ = f;
}

Layer::~Layer() {
    if(nin_>0 || nout_>0) {
        W_.clear();
        b_.clear();
        X_.clear();
        Z_.clear();
        A_.clear();
        Delt_.clear();
    }
}

void Layer::Eval(vector<double> X) {
    if(X.size()!=nin_)
        ErrorMsg("Error in Eval: input and neuron don't have the same size");
    X_ = X;
    for(size_t i=0; i<nout_; ++i) {
        Z_.at(i) = b_.at(i);
        for(size_t j=0; j<nin_; ++j)
            Z_.at(i) += W_.at(i*nin_+j)*X.at(j);
    }
    switch(f_) {
        case Logistic:
            for(size_t i=0; i<nout_; ++i)
                A_.at(i) = 1.0/(1.0+exp(-Z_.at(i)));
            break;
        case ATan:
            for(size_t i=0; i<nout_; ++i)
                A_.at(i) = (2.0/M_PI)*atan(M_PI*Z_.at(i)/2.0);
            break;
        case ReLU:
            for(size_t i=0; i<nout_; ++i) {
                if(Z_.at(i)<0)
                    A_.at(i) = 0;
                else
                    A_.at(i) = Z_.at(i);
            }
            break;
        case ELU1:
            for(size_t i=0; i<nout_; ++i) {
                if(Z_.at(i)<0)
                    A_.at(i) = exp(Z_.at(i))-1.0;
                else
                    A_.at(i) = Z_.at(i);
            }
            break;
        default:
            ErrorMsg("Layer::Eval: Unknown activation function");
    }
}

double Layer::fdiff(int i) {
    double df;
    switch(f_) {
        case Logistic:
            df  = A_.at(i)*(1.0-A_.at(i));
            break;
        case ATan:
            df = 1.0/(1.0+pow(M_PI*Z_.at(i)/2.0,2));
            break;
        case ReLU:
            df = (Z_.at(i)<0)?0.0:1.0;
            break;
        case ELU1:
            df = (Z_.at(i)<0)?(A_.at(i)+1.0):1.0;
            break;
        default:
            ErrorMsg("Layer::SetDelta: Unknown activation function");
    }
    return df;
}

void Layer::SetDelta(vector<double> Delt) {
    if(Delt.size()!=nout_)
        ErrorMsg("Layer::SetDelta: wrong size of input");
    for(size_t j=0; j<nout_; ++j)
                Delt_.at(j) = Delt.at(j)*fdiff(j);
}

vector<double> Layer::GetDelta() {
    vector<double> Deltnew(nin_);
    for(size_t i=0; i<nin_; ++i) {
        Deltnew.at(i) = 0.0;
        for(size_t j=0; j<nout_; ++j)
            Deltnew.at(i) += W_.at(j*nin_+i)*Delt_.at(j);
    }
    return Deltnew;
}

void Layer::UpGrade(double lamb) {
    for(size_t i=0; i<nout_; ++i) {
        b_.at(i) -= lamb*Delt_.at(i);
        for(size_t j=0; j<nin_; ++j)
            W_.at(i*nin_+j) -= lamb*Delt_.at(i)*X_.at(j);
    }
}

void Layer::Save(const std::string& fname) {
    ofstream outfile;
    outfile.open(fname);
    for(size_t i=0; i<nout_; ++i) {
        for(size_t j=0; j<nin_; ++j)
            outfile<<W_.at(i*nin_+j)<<"\t";
        outfile<<b_.at(i)<<endl;
    }
    outfile.close();
}
