//
//  test_IO.cpp
//  FeedForward
//
//  Created by Jeremie Bec
//

#include "src/Network.hpp"
#include "basics/RunsIO.hpp"
#include "basics/Arglist.hpp"

using namespace std;

int main(int argc, char* argv[]) {
    WriteProcessInfo(argc, argv);
    
    string purpose("Some tests on reading/writing neuralnet.");

    ArgList args(argc, argv, purpose);
    
    // Read the command-line options
    args.section("Program options");
    const string outdir = args.getpath("-o", "--outdir", "test/", "output directory");
    const int Nlayer = args.getint("-nl", "--nlayer", 2, "number of internal layers");
    const int Nneurons = args.getint("-nn", "--nneuron", 2, "number of neurons per layer");
    const int Ntest = args.getint("-nt", "--ntest", 2, "number of networks tested");
    args.check();
    mkdir(outdir);
    args.save(outdir);
    
    // First test: writing
    cout<<endl<<"--------------------------------------"<<endl;
    cout<<"Generating a random network with 2 inputs and 1 output"<<endl;
    cout<<"number of internal layers:     "<<Nlayer<<endl;
    cout<<"number of neuron per layer:    "<<Nneurons<<endl;
    
    // set the random seed
    std::default_random_engine rng;
    time_t t;
    rng.seed((unsigned) time(&t));
    std::string fname = outdir+"net0";
    vector<double> x0(2),x1;
    x0.at(0) = 0.12; x0.at(1) = 0.22;

    cout<<"Writing in file "<<fname<<endl;
    for(int i=0; i<Ntest; ++i) {
        Network NN(2,1,Nneurons,Nlayer,rng);
        NN.Save(fname);
        x1 = NN.Eval(x0);
        cout<<"Result of the net: "<<x1.at(0)<<endl;
    }
    cout<<"Reading from file "<<fname<<endl;
    char cname[512];
    strcpy(cname, fname.c_str());
    FILE *file_in = fopen(cname,"r");
    for(int i=0; i<Ntest; ++i) {
        Network NN1(file_in);
        x1 = NN1.Eval(x0);
        cout<<"Result of the net: "<<x1.at(0)<<endl;
    }
    fclose(file_in);
    
    return 1;
}
