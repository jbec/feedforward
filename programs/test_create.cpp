//
//  learn.cpp
//  FeedForward
//
//  Created by Jeremie Bec
//

#include "src/Network.hpp"
#include "basics/RunsIO.hpp"
#include "basics/Arglist.hpp"

using namespace std;

int main(int argc, char* argv[]) {
    WriteProcessInfo(argc, argv);
    
    string purpose("Some tests on nework generation and writing to the hard drive.");

    ArgList args(argc, argv, purpose);
    
    // Read the command-line options
    args.section("Program options");
    const string outdir = args.getpath("-o", "--outdir", "data/", "output directory");
    const int Nlayer = args.getint("-nl", "--nlayer", 2, "number of internal layers");
    const int Nneurons = args.getint("-nn", "--nneuron", 2, "number of neurons per layer");
    args.check();
    mkdir(outdir);
    args.save(outdir);
    
    // define the potential field
    cout<<endl<<"--------------------------------------"<<endl;
    cout<<"Generating a random network with 2 inputs and 1 output"<<endl;
    cout<<"number of internal layers:     "<<Nlayer<<endl;
    cout<<"number of neuron per layer:    "<<Nneurons<<endl;
    
    // set the random seed
    std::default_random_engine rng;
    time_t t;
    rng.seed((unsigned) time(&t));
    
    Network NN(2,1,Nneurons,Nlayer,rng);
    NN.Save(outdir+"net0");
    return 1;
}
