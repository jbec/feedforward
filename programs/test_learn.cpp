//
//  learn_parabola.cpp
//  FeedForward
//
//  Created by Jeremie Bec
//

#include "src/Network.hpp"
#include "basics/RunsIO.hpp"
#include "basics/Arglist.hpp"

using namespace std;

int main(int argc, char* argv[]) {
    WriteProcessInfo(argc, argv);
    
    string purpose("Learning to predict from a 2D input.");

    ArgList args(argc, argv, purpose);
    
    // Read the command-line options
    args.section("Program options");
    const string outdir = args.getpath("-o", "--outdir", "data/", "output directory");
    const int Nlayer = args.getint("-nl", "--nlayer", 2, "number of internal layers");
    const int Nneurons = args.getint("-nn", "--nneuron", 8, "number of neurons per layer");
    const int Nit = args.getint("-nt", "--nit", 1000, "number of epochs");
    const int Nout = args.getint("-nout", "--nout", 5000, "number of iterations per epoch");
    const int Nreal = args.getint("-nr", "--nreal", 1, "number of realisations");
    const int Nx = args.getint("-nx", "--nsamp", 100, "number of sampling point for the output");
    const double lamb = args.getreal("-l", "--learningrate", 0.01, "learnong rate of backward propagation");
    const int idfun = args.getint("-fun", "--test_fun", 0, "type of tests (0: linear, 1:parabola, 2:circle, 3:fractal)");
    args.check();
    mkdir(outdir);
    args.save(outdir);
    
    cout<<endl<<"--------------------------------------"<<endl;
    cout<<"Generating a random network with 2 inputs and 1 output"<<endl;
    cout<<"number of internal layers:     "<<Nlayer<<endl;
    cout<<"number of neurons per layer:   "<<Nneurons<<endl;
    
    // Assign the type of test function
    testfun fun;
    switch (idfun) {
        case 0:
            fun = linear;
            cout<<"linear test function x_2 = x_1"<<endl;
            break;
        case 1:
            fun = parabola;
            cout<<"quadratic test function x_2 = 4*x_1*(1-x_1)"<<endl;
            break;
        case 2:
            fun = circle;
            cout<<"circle test function (x_1-0.5)^2+(x_2-0.5)^2 = 0.5^2"<<endl;
            break;
        case 3:
            fun = fractal;
            cout<<"fractal basin boundary"<<endl;
            break;
        default:
            ErrorMsg("Unknown test function");
            break;
    }
    
    // set the random seed
    std::default_random_engine rng1;
    std::default_random_engine rng2;
    time_t t;
    //rng.seed((unsigned) time(&t));
    rng1.seed((unsigned) time(&t));
    rng2.seed((unsigned) time(&t));
    std::uniform_real_distribution<double> Unif(0.0,1.0);
    vector<double> x(2), y(1);
    vector<double> res;
    vector<double> l2(Nreal*(Nit+1));
    
    ofstream outglobal;
    outglobal.open(outdir+"error.txt");
    
    for(int ireal=0; ireal<Nreal; ++ireal) {
        Network NN(2,1,Nneurons,Nlayer,rng1,fun);
        l2.at(ireal*(Nit+1)) = NN.calcL2(Nx);
        for(int iep=0; iep<Nit; ++iep) {
            for(int it=0; it<Nout; ++it) {
                x.at(0) = Unif(rng2);
                x.at(1) = Unif(rng2);
                y.at(0) = NN.test_fun(x);
                res = NN.Eval(x);
                NN.BackProp(y,res,lamb);
            }
            l2.at(ireal*(Nit+1)+iep+1) = NN.calcL2(Nx);
            cout<<iep<<"\t"<<l2.at(ireal*(Nit+1)+iep+1)<<endl;
            
        }
        for(int iep=0; iep<Nit+1; ++iep)
            outglobal<<l2.at(ireal*(Nit+1)+iep)<<"\t";
        outglobal<<endl;
        outglobal.flush();
        
        ofstream outfile, out1, out2;
        outfile.open(outdir+"map"+i2s(ireal)+".txt");
        out1.open(outdir+"derr_x1_"+i2s(ireal)+".txt");
        out2.open(outdir+"derr_x2_"+i2s(ireal)+".txt");
        for(int ix=0; ix<Nx; ++ix) {
            for(int iy=0; iy<Nx; ++iy) {
                x.at(0) = (double)ix/(double)(Nx-1);
                x.at(1) = (double)iy/(double)(Nx-1);
                y.at(0) = NN.test_fun(x);
                res = NN.Eval(x);
                outfile<<res[0]<<" ";
                res = NN.FwdError();
                out1<<res[0]<<" ";
                out2<<res[1]<<" ";
            }
            outfile<<endl;
            out1<<endl;
            out2<<endl;
        }
        outfile.close();
        out1.close();
        out2.close();
        
    }
    outglobal.close();
}
