//
//  calc_L2_fromfile.cpp
//  FeedForward
//
//  Created by Jeremie Bec on 19/01/2021.
//

#include "src/Network.hpp"
#include "basics/RunsIO.hpp"
#include "basics/Arglist.hpp"

using namespace std;

int main(int argc, char* argv[]) {
    WriteProcessInfo(argc, argv);
    
    string purpose("Compute L2 error from saved data.");

    ArgList args(argc, argv, purpose);
    
    // Read the command-line options
    args.section("Program options");
    const string indir = args.getpath("-i", "--indir", "data/", "input directory");
    const int Nx = args.getint("-nx", "--nsamp", 100, "number of sampling point to compute the L2 norm");
    const int idfun = args.getint("-fun", "--test_fun", 0, "type of tests (0: linear, 1:parabola, 2:circle, 3:fractal)");
    const int Nit = args.getint("-nt", "--nit", 1000, "number of epochs");
    args.check();
    args.save(indir);
    
    cout<<endl<<"--------------------------------------"<<endl;
    cout<<"Calculating error for "<<Nit<<" epochs"<<endl;
    cout<<"from directory "<<indir<<endl;

    // Assign the type of test function
    testfun fun;
    switch (idfun) {
        case 0:
            fun = linear;
            cout<<"linear test function x_2 = x_1"<<endl;
            break;
        case 1:
            fun = parabola;
            cout<<"quadratic test function x_2 = 4*x_1*(1-x_1)"<<endl;
            break;
        case 2:
            fun = circle;
            cout<<"circle test function (x_1-0.5)^2+(x_2-0.5)^2 = 0.5^2"<<endl;
            break;
        case 3:
            fun = fractal;
            cout<<"fractal basin boundary"<<endl;
            break;
        default:
            ErrorMsg("Unknown test function");
            break;
    }
    
    std::string fname = indir+"net"+i2s(0);
    char cname[512];
    strcpy(cname, fname.c_str());
    FILE *file_in = fopen(cname,"r");
    Network NN = Network(file_in);
    if(NN.Nin()==0)
        ErrorMsg("Nothing read...");
    cout<<"Read a network with "<<NN.Nin()<<" inputs and "<<NN.Nout()<<" output"<<endl;
    cout<<"number of internal layers:     "<<NN.Nlayer()<<endl;
    cout<<"number of neurons per layer:   "<<NN.Nneurons()<<endl;
    
    int Nreal = 1;
    do {
        NN = Network(file_in);
        Nreal++;
    } while (NN.Nin()>0);
    Nreal--;
    cout<<"detected "<<Nreal<<" realizations"<<endl;
    fclose(file_in);
    
    vector<double> l2(Nreal*Nit);
    
    ofstream outglobal;
    outglobal.open(indir+"error.txt");
    
    for(int iep=0; iep<Nit; ++iep) {
        fname = indir+"net"+i2s(iep);
        strcpy(cname, fname.c_str());
        file_in = fopen(cname,"r");
        outglobal<<iep;
        for(int ireal=0; ireal<Nreal; ++ireal) {
            NN = Network(file_in,fun);
            l2.at(ireal*Nit+iep) = NN.calcL2(Nx);
            outglobal<<"\t"<<l2.at(ireal*Nit+iep);
        }
        outglobal<<endl;
        for(int iep=0; iep<Nit+1; ++iep)
        outglobal.flush();
    }
    outglobal.close();
}
